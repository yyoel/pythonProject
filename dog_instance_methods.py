class Dog:
        species = 'mammal'
        # initializer
        def __init__(self, name, age):
            self.name = name
            self.age = age
        # instance method
        def decription(self):
            return "{} is {} years old".format(self.name, self.age)

        def speak(self, sound):
            return "{} says {}".format(self.name, sound)

# instantiate the Dog object
mikey = Dog("Mikey", 6)

# call instance methods
print(mikey.decription())
print(mikey.speak("bark"))
