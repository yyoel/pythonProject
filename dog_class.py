class Dog:
        # Class Attribute
        species = 'mammal'

        # Initializer / Instance Attributes
        def __init__(self, name, age):
            self.name = name
            self.age = age

# Instantiate the Dog
philo = Dog("Philo", 1)
mon = Dog("Mon", 6)
monmon = Dog("MonMon", 7)

# Access the instance attributes
print("{} is {} and {} is {}.".format(
    philo.name, philo.age, monmon.name, monmon.age))

def get_biggets_number(*args):
    return max(*args)

print("The oldest dog is {} years old".format(
       get_biggets_number(philo.age, mon.age, monmon.age)))

# Is Philo a mammal?
if philo.species == "mammal":
    print("{0} is a {1}!".format(philo.name, philo.species))

